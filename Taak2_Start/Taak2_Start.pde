//Coördinates of cube
int middleX, middleY, top, bottom, left, right, topCorner, bottomCorner;

//Variables for myAnimation
int size = 10;
boolean playMyAnimation = true;

void setup() {
  size(1280, 720); 
  colorMode(HSB, 360, 100, 100, 100);
  setGlobalVariables();
  initializeMinim();
}

void draw() {
  //White Cube planes
  background(0, 0, 100);
  //checks each frame if there is a beat.
  beat.detect(player.mix);

  drawAnimations();
  drawCubeMask();
}

void keyReleased() {
  //toggle Animations ON or OFF
  if (key == 'u') {
    playMyAnimation = !playMyAnimation;
  }
}

void drawAnimations()
{  
  //play animation if toggled ON
  if (playMyAnimation == true) {
    myAnimation();
  }
}

void myAnimation() {
  if (beat.isOnset()) {
    size = 10;
  }

  noStroke();
  fill(frameCount % 360, 100, 100);
  ellipse(middleX, middleY, size, size);

  size+= 20;
}